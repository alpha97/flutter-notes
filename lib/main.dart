import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:notes_app/addNote.dart';
import 'package:notes_app/editNote.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {

  MyHomePage({Key key, this.title}) : super(key: key);
  CollectionReference ref = FirebaseFirestore.instance.collection("notes");
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final ref = FirebaseFirestore.instance.collection("notes");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text("My Notes"),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute( builder: (_) => AddNote()));
        },
      ),
      body: StreamBuilder(
        stream: ref.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          return GridView.builder(gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
              itemCount: snapshot.hasData?  snapshot.data.docs.length : 0,
              itemBuilder: (_,index){
            return GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute( builder: (_) => EditNote(docToEdit: snapshot.data.docs[index],)));
              },
              child: Container(
              margin: EdgeInsets.all(10.0),
              height: 150,
              color: Colors.grey[200],
                child: Column(
                  children: [
                    Text(snapshot.data.docs[index].get('title')),
                    Text(snapshot.data.docs[index].get('content'))
                  ],
                ),
              ),
            );
    }
    );
        }
      )
    );
  }
}