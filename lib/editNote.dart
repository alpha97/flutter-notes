import 'package:cloud_firestore/cloud_firestore.dart';
import "package:flutter/material.dart";

class EditNote extends StatefulWidget{

  DocumentSnapshot docToEdit;
  EditNote({this.docToEdit});

  @override
  _EditNoteState createState() => _EditNoteState();
}

class _EditNoteState extends State<EditNote>{

  TextEditingController title = TextEditingController();
  TextEditingController content = TextEditingController();


  @override
  void initState() {
    title = TextEditingController(text: widget.docToEdit.data()['title']);
    content = TextEditingController(text: widget.docToEdit.data()['content']);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notes"),
        actions: [FlatButton(onPressed: () {
          // ref.add({"title": title.text ,
          //   "content": content.text}).whenComplete(() => Navigator.pop(context));
        },
            child: Text("Save"))],
      ),
      body: Column(children: [
        Container(
            decoration: BoxDecoration(border: Border.all()),
            child: TextField(controller: title ,decoration: InputDecoration(hintText: "Title"))),
        Expanded(
          child: Container(
              decoration: BoxDecoration(border: Border.all()),
              child: TextField(
                  controller: content,
                  maxLines: null,
                  expands: true ,
                  decoration: InputDecoration(hintText: "Content")
              )
          ),
        )
      ]),
    );
  }
}